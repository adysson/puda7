class AddActiveToPages < ActiveRecord::Migration
  def change
    add_column :pages, :active, :integer, limit: 1, default: 0
  end
end
