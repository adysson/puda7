namespace :seeds do

  desc "Create startup entries for website to work properly"
  task :startup_seeds => :environment do
    User.create(
      name: "Pája", 
      email: "p.radochov@gmail.com", 
      password: 1,
      password_digest: "$2a$10$4CrsoMgzvBVUsHFTwZBNJOVsvpIg2V7/tOGTjDLX1.mELXjhTW7kS"
    )    
  
    puts "All startup entries created."
  end
end