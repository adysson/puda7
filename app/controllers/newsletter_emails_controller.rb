class NewsletterEmailsController < WebsiteApplicationController
  before_action :set_newsletter_email, only: :destroy
  before_action :authorize, except: :create
  
  def index
    @newsletter_emails = NewsletterEmail.all
  end
  
  def create
    @first_email = !NewsletterEmail.any?
    @newsletter_email = NewsletterEmail.new(email: params[:email])
    
    respond_to do |format|
      if @newsletter_email.save
        message = current_user.present? ? t('newsletter_emails.controller.create_ok_admin') 
          : t('newsletter.application_success')
        format.html { redirect_to :back, notice: message }
        format.js
        format.json { render json: @newsletter_email, status: :created, location: @newsletter_email }
      else
        format.html { redirect_to :back, notice: t('newsletter.application_error') }
        format.js
        format.json { render json: @newsletter_email.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @newsletter_email.destroy
    @no_emails = !NewsletterEmail.any?
    respond_to do |format|
      format.html { redirect_to newsletter_emails_url, notice: t('newsletter_emails.controller.delete_ok') }
      format.js
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_newsletter_email
    @newsletter_email = NewsletterEmail.find(params[:id])
  end
end
