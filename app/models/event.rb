class Event < ActiveRecord::Base
  validates_presence_of :title, :body, :event_type, :date_vernisage
  validates_presence_of :date_start, :date_end, if: :exhibition?
  
  enum event_type: [:exhibition, :concert, :workshop, :screening]
  
  def dates_line
    if exhibition?
      I18n.t('events.date_line.exhibition', 
              date_start: date_start.strftime('%d.%m.'), 
              date_end: date_end.strftime('%d.%m.'),
              date_vernisage: date_vernisage.strftime('%d.%m.%Y v %H:%M'))
    else
      date_vernisage.strftime('%d.%m.%Y v %H:%M')
    end
  end
end
