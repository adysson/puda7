// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery-z-timepicker
//= require jquery_ujs
//= require pages
//= require turbolinks
//= require ckeditor/init
$(document).ready(function() {
  $(function() {
      $('.datepicker').datepicker({ dateFormat: "dd. mm. yy" });
      $('.datetimepicker').datetimepicker({ dateFormat: "dd. mm. yy" });
      $('.timepicker').timepicker();
  });

  $('.pages_controller table.index tbody').sortable({ items: '> tr'});

  if ($('#event_event_type').val() != 'exhibition') {
    $('div#date_start_end').css('display', 'none');
  }
  
  $('#event_event_type').change(function() {  
    if ($(this).val() == 'exhibition') {
      $('div#date_start_end').css('display', 'block');
    }
    else {
      $('div#date_start_end').css('display', 'none');
    }
  });
});
