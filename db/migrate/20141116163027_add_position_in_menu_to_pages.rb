class AddPositionInMenuToPages < ActiveRecord::Migration
  def change
    add_column :pages, :position_in_menu, :integer, limit: 2, default: 0
  end
end
