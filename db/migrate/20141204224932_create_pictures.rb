class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.attachment :picture
      t.integer :original_width
      t.integer :original_height
      t.string :description

      t.timestamps
    end
  end
end
