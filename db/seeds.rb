# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# run when CKeditor used from Git repo, not installing fresh one on app 1st setup
Ckeditor::Asset.create(
  id: 1, 
  data_file_name: 'fotbal_patri_za_luzanky.jpg', 
  data_content_type: 'image/jpeg', 
  data_file_size: 456085, 
  type: 'Ckeditor::Picture', 
  width: 1000, 
  height: 857, 
  created_at: Time.now, 
  updated_at: Time.now
)