class RemoveWidthAndHeightFromPicturesAndAddDimensions < ActiveRecord::Migration
  def change
    remove_column :pictures, :original_width, :integer
    remove_column :pictures, :original_height, :integer
    add_column :pictures, :dimensions, :string, limit: 30
  end
end
