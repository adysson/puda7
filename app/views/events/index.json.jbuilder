json.array!(@events) do |event|
  json.extract! event, :id, :title, :body, :date_start, :date_end, :date_vernisage
  json.url event_url(event, format: :json)
end
