json.array!(@pages) do |page|
  json.extract! page, :id, :title, :body, :show_in_menu
  json.url page_url(page, format: :json)
end
