# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141205034841) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "events", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "date_start"
    t.datetime "date_end"
    t.datetime "date_vernisage"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_type"
  end

  create_table "newsletter_emails", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.boolean  "show_in_menu"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "page_url",         limit: 150
    t.integer  "position_in_menu", limit: 2,   default: 0
    t.integer  "active",           limit: 2,   default: 0
  end

  create_table "pictures", force: true do |t|
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dimensions",           limit: 30
  end

  create_table "users", force: true do |t|
    t.string   "name",            limit: 20
    t.string   "email",           limit: 40
    t.string   "password_digest", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
