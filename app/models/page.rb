class Page < ActiveRecord::Base
  def inactive?
    active == 0
  end
  
  def self.pages_for_menu
    where('show_in_menu = ?', true).select('title, page_url, body').order('position_in_menu')
  end
  
  def to_param
    page_url
  end
end
