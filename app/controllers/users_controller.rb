class UsersController < WebsiteApplicationController
  def new
    @user = User.new
  end
  
  def create
    user = User.new(user_params)
    
    if user.save
      redirect_to root_url, notice: "Tak se nám zaregistrovala"
    else
      flash.now.alert("Hele, něco se dojebalo. Ještě to zkus ;]")
      render "new"
    end
  end
  
  private
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
