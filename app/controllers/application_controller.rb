class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :current_user
  before_action :set_stylesheet, :set_title
  rescue_from 'ActiveRecord::RecordNotFound', with: :render_404
  
  protected
  
  def render_404
    render status: '404', text: t('pages.not_found')
  end
  
  private
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user
  
  def authorize
    redirect_to root_url if current_user.blank?
  end
  
  def set_title
    @title = "půda 7, bytová galerie v Brně"
  end
  
  def set_stylesheet
    @stylesheet = params[:no_photo].present? ? 'application_no_photo' : 'application'
  end
end
