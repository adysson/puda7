class PagesController < WebsiteApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authorize, except: [:show]

  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all.order("page_url")
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to specific_page_path(@page), notice: t('pages.controller.save_ok') } # originally redirected to @page
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to specific_page_path(@page), notice: t('pages.controller.save_ok') } #originally redirected to @page
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: t('pages.controller.delete_ok') }
      format.json { head :no_content }
    end
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      if current_user.present?
        @page = Page.find_by_page_url!(params[:id])
      else
        @page = Page.where(active: 1).find_by_page_url!(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(
        :title, 
        :page_url, 
        :body, 
        :show_in_menu, 
        :position_in_menu,
        :active
      )
    end
end
