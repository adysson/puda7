class AddUrlToPages < ActiveRecord::Migration
  def change
    add_column :pages, :page_url, :string, limit: 150
  end
end
