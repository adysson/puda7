class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :body
      t.datetime :date_start
      t.datetime :date_end
      t.datetime :date_vernisage

      t.timestamps
    end
  end
end
