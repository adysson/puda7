class Ckeditor::Picture < Ckeditor::Asset
  has_attached_file :data,
                    :url  => "/websites/puda7/:id_:basename_:style.:extension",
                    :path => "/websites/puda7/:id_:basename_:style.:extension",
                    :styles => { :content => '550>', :thumb => '118x100#' },
                    :storage => :dropbox,
                    :dropbox_credentials => Rails.root.join("config/dropbox.yml"),
                    :dropbox_visibility => "public"

  validates_attachment_presence :data
  validates_attachment_size :data, :less_than => 2.megabytes
  validates_attachment_content_type :data, :content_type => /\Aimage/

  def url_content
    url(:content)
  end
end
