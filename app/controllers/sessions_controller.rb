class SessionsController < WebsiteApplicationController
  def new
    redirect_to root_url, notice: "už seš přihlášená.." if current_user.present?
    @user = User.new
  end
  
  def create
    user = User.find_by_email(params[:email])
    
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "seš přihlášená, galeristko ;]"
    else
      flash.now.alert = "zadalas' špatný heslo, galeristko :]"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "seš odhlášená, galeristko :]"
  end
end
