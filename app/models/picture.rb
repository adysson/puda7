class Picture < ActiveRecord::Base
  has_attached_file :picture, 
    styles: { thumb: "150x", medium: "550x", large: "900x" }
  before_save :extract_dimensions
  
  validates_attachment :picture,
    presence: true, 
    content_type: { content_type: ['image/jpeg', 'image/png', 'image/gif'] },
    size: { in: 0..1.megabytes },
    file_name: { matches: [/\.png\Z/, /\.jpe?g\Z/, /\.gif\Z/]}
  
  def extract_dimensions
    tempfile = picture.queued_for_write[:original]
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.dimensions = "#{geometry.width.to_i}x#{geometry.height.to_i}px"
    end
  end
end