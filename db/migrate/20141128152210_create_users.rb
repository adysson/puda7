class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, limit: 20
      t.string :email, limit: 40
      t.string :password_digest, limit: 64

      t.timestamps
    end
  end
end
