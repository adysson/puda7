class WebsiteApplicationController < ApplicationController
  before_action :set_user_menu, :set_main_menu
  
  private
  
  def set_main_menu
    @main_menu = []
    @main_menu << {anchor_text: t('menu.programme'), url: events_path}
    
    Page.pages_for_menu.each do |page|
      @main_menu << {anchor_text: page.title, url: specific_page_path(page)}
    end
  end
  
  def set_user_menu
    @user_menu = []
    if current_user.present?
      @user_menu << {
        anchor_text: t('events.new'),
        url: new_event_url
      }
      @user_menu << {
        anchor_text: t('pages.new'),
        url: new_page_url
      }
      @user_menu << {
        anchor_text: t('menu.newsletter'),
        url: newsletter_emails_url
      }
      @user_menu << {
        anchor_text: t('menu.logout'), 
        url: logout_url, 
        options: { method: :delete }
      }
    end
  end
end
